
$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000 //cada dos segundos
    });

    $('#contacto').on('show.bs.modal', function(e){
      //  console.log('El Modal Contacto');
        $('#btnContacto').removeClass('btn-outline-success');
        $('#btnContacto').addClass('btn-primary');
    });

    $('#btnCerrar').click(function(){
        $('#btnContacto').removeClass('btn-primary');
        $('#btnContacto').addClass('btn-outline-success');        
    });


});